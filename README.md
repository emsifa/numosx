Numosx
===================================================================
GTK Theme like OS-X Yosemite based on Numix.
Numix is a part of the [Numix Project](http://numixproject.org).

### Requirements

GTK+ 3.6 or above

Murrine theme engine

### Screenshoots

![terminal](https://dl.dropboxusercontent.com/u/102070675/ss-numosx/1.png)

![nautilus](https://dl.dropboxusercontent.com/u/102070675/ss-numosx/2.png)

![inkscape](https://dl.dropboxusercontent.com/u/102070675/ss-numosx/3.png)

![system settings](https://dl.dropboxusercontent.com/u/102070675/ss-numosx/4.png)

### Code and license

Report bugs or contribute at [GitHub](https://github.com/shimmerproject/Numix)

License: GPL-3.0+
